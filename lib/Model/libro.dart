import 'package:firebase_database/firebase_database.dart';


class Book {
  String _id;
  String _name;
  String _area;
  String _description;
  String _serie;
  String _nombreApp;
  String _package;
  String _productImage;

  Book(this._id,this._name,this._area,this._description,
      this._serie,this._nombreApp,this._package,this._productImage);

  Book.map(dynamic obj){
    this._name = obj['name'];
    this._area = obj['area'];
    this._description = obj['description'];
    this._serie = obj['serie'];
    this._nombreApp = obj['nombreApp'];
    this._package = obj['package'];
    this._productImage = obj['ProductImage'];
  }

  String get id => _id;
  String get name => _name;
  String get area => _area;
  String get description => _description;
  String get serie => _serie;
  String get nombreApp => _nombreApp;
  String get package => _package;
  String get productImage => _productImage;

  Book.fromSnapShot(DataSnapshot snapshot){
    _id = snapshot.key;
    _name = snapshot.value['name'];
    _area = snapshot.value['area'];
    _description = snapshot.value['description'];
    _serie = snapshot.value['serie'];
    _nombreApp = snapshot.value['nombreApp'];
    _package = snapshot.value['package'];
    _productImage = snapshot.value['ProductImage'];
  }
}