import 'package:flutter/material.dart';
import 'package:appeducar/behaviors/hiddenScrollBehaviour.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>(); // para presentar menus contextuales con informacion al usuario



  String _email;
  String _password;


  bool _isLoggingIn = false;



  _login() async {
    // para identificar si es está ejecutando el metodo, si es verdadero detiene
    if (_isLoggingIn) return;
    setState(() {
      _isLoggingIn = true;
    });

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Login user.."),
    ));

    final form = _formKey.currentState;

    if (!form.validate()) {
      // llama a cada uno de los formfields en su metodo de validate al igual que a cada uno de los de save
      _scaffoldKey.currentState.hideCurrentSnackBar();
      setState(() {
        _isLoggingIn = false;
      });
      return;
    }

    form.save();

    try {
      // para hacer referencia al metodo de autenticacion de la base de datos
      await FirebaseAuth.instance.signInWithEmailAndPassword(email: _email, password: _password);
      Navigator.of(context).pop();
      if(_email == "educar.webmaster@gmail.com"){
        Navigator.of(context).pushReplacementNamed('/maintabs');
      }else{
        Navigator.of(context).pushReplacementNamed('/maintabs2');
      }


    } catch (ev) {
      _scaffoldKey.currentState.hideCurrentSnackBar();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(ev.message),
        duration: Duration(seconds: 10),
        action: SnackBarAction(
          label: "Dismiss",
          onPressed: () {
            _scaffoldKey.currentState.hideCurrentSnackBar();
          },
        ),
      ));
    }
    finally{
      setState(() {
        _isLoggingIn = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Container(
          padding: EdgeInsets.all(20.0),
          child: ScrollConfiguration(
            behavior: HiddenScrollBehavior(),
            child: Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    /*FlutterLogo(
                      style: FlutterLogoStyle.horizontal,
                      size: 200.0,
                    ),*/
                    TextFormField(
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(labelText: 'Email'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Correo incorrecto";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        // metodo quese llama cuando se guarda el nombre
                        setState(() {
                          _email = val;
                        });
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(labelText: 'Password'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Contraseña incorrecta";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        setState(() {
                          _password = val;
                        });
                      },
                    ),
                  ],
                )),
          )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          _login();
        },
        child: Icon(Icons.account_circle),
      ),
      persistentFooterButtons: <Widget>[ // para ir a la pagina de login
        FlatButton(
            onPressed: (){
              Navigator.of(context).pushNamed('/register');

            },
            child: Text("Crear cuenta")
        ),
        FlatButton(
          child: Text("¿Has olvidado tu contraseña?"),
          onPressed: (){
            Navigator.of(context).pushNamed('/forgotpassword');
          },
          textColor: Colors.blueGrey,
        )
      ],
    );
  }
}
