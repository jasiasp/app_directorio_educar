import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:url_launcher/url_launcher.dart';

const URL = "https://www.educar.comc.co";


class SettingsPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _SettingsPagePageState();


}

class _SettingsPagePageState extends State<SettingsPage>{


  _gotoUrl()async{
    const url = 'https://www.educar.com.co';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _logout()async{
    await FirebaseAuth.instance.signOut();
    Navigator.of(context)..pushReplacementNamed('/login');
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajustes"),
      ),
      body: Center(
        child: ListView.builder(
            itemCount: 1,
            padding: EdgeInsets.only(top: 3.0),
            itemBuilder:(context,position){
              return Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: (){
                      _logout();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('Salir'),
                        Icon(Icons.exit_to_app),
                      ],
                    ),
                  ),
                  RaisedButton(
                    onPressed: (){
                      _gotoUrl();
                    },
                    child: new Text('Show Flutter ññhomepage'),
                  ),
                ],
              );
            }
        ),
        /*child: FlatButton(
          onPressed: (){
            _logout();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Salir'),
              //Spacer(),
              Icon(Icons.exit_to_app),
            ],
          ),
        ),*/
      ),
    );
  }

}