import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:appeducar/behaviors/hiddenScrollBehaviour.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:http/http.dart' as http;

//


class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>(); // para presentar menus contextuales con informacion al usuario
  final DatabaseReference database = FirebaseDatabase.instance.reference().child("Tabla de registros");


  String _email;
  String _password;
  String _bookCode;
  String _name;
  String _date;

  bool _isRegistering = false;
  TextEditingController codeController = new TextEditingController();

  Map data;
  List codeData;

  Future<String> getData() async{
    //print(codeController.text);
    http.Response response = await http.get(

      Uri.encodeFull("http://sie.educar.com.co/webresources/api/diario_lectura/consult-code?codigo="+codeController.text),
      headers: {
        "Accept":"application/json"
      }
    );

    data = json.decode(response.body);

    print(data.toString());

    if(data == null){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Book code incorrect, please try it again"),
        duration: Duration(seconds: 10),
        action: SnackBarAction(
          label: "Dismiss",
          onPressed: () {
            _scaffoldKey.currentState.hideCurrentSnackBar();
          },
        ),
      ));
    }else{
      if(data["state"] == 1){
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("This book code already exists "),
          duration: Duration(seconds: 10),
          action: SnackBarAction(
            label: "Dismiss",
            onPressed: () {
              _scaffoldKey.currentState.hideCurrentSnackBar();
            },
          ),
        ));
      }
    }
  }


  _register() async {
    var now = new DateTime.now();
    _date = now.day.toString()+"/"+now.month.toString()+"/"+now.year.toString();
    // para identificar si es está ejecutando el metodo, si es verdadero detiene
    if (_isRegistering) return;
    setState(() {
      _isRegistering = true;
    });

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Registering user.."),
    ));

    final form = _formKey.currentState;

    if (!form.validate()) {
      // llama a cada uno de los formfields en su metodo de validate al igual que a cada uno de los de save
      _scaffoldKey.currentState.hideCurrentSnackBar();
      setState(() {
        _isRegistering = false;
      });
      return;
    }

    form.save();

    try {
      // para hacer referencia al metodo de autenticacion de la base de datos
      //getData();
      await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _password);
      database.push().set({

        'nombre':_name,
        'email': _email,
        'book_code': _bookCode,
        'date':_date,
      });

      Navigator.of(context).pushReplacementNamed('/maintabs');

    } catch (ev) {
      _scaffoldKey.currentState.hideCurrentSnackBar();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(ev.message),
        duration: Duration(seconds: 10),
        action: SnackBarAction(
          label: "Dismiss",
          onPressed: () {
            _scaffoldKey.currentState.hideCurrentSnackBar();
          },
        ),
      ));
    }
    finally{
      setState(() {
        _isRegistering = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Registro'),
      ),
      body: Container(
          padding: EdgeInsets.all(20.0),
          child: ScrollConfiguration(
            behavior: HiddenScrollBehavior(),
            child: Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    FlutterLogo(
                      style: FlutterLogoStyle.horizontal,
                      size: 200.0,
                    ),
                    TextFormField(
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(labelText: 'Name'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Please enter a valid name";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        // metodo quese llama cuando se guarda el nombre
                        setState(() {
                          _name = val;
                        });
                      },
                    ),
                    TextFormField(
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(labelText: 'Email'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Por favor introduzca un correo electrónico válido";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        // metodo quese llama cuando se guarda el nombre
                        setState(() {
                          _email = val;
                        });
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(labelText: 'Password'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Contraseña incorrecta";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        setState(() {
                          _password = val;
                        });
                      },
                    ),
                    TextFormField(
                      controller:codeController,
                      decoration: InputDecoration(labelText: 'Book Code'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Pin invalido";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        setState(() {
                          _bookCode = val;
                        });
                      },
                    ),
                  ],
                )),
          )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
         // _register();
          getData();

        },
        child: Icon(Icons.person_add),
      ),
      persistentFooterButtons: <Widget>[
        // para ir a la pagina de login
        FlatButton(
            onPressed: () {
              Navigator.of(context).pop(); // para eliminar la pila de paginas e ir a la pagina anterior // se definio la ruta en el archivo de route.dart
            },
            child: Text("Ingresar"))
      ],
    );
  }
}


