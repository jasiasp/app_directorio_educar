import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:appeducar/Model/libro.dart';


class ProductInformation extends StatefulWidget {
  final Book product;
  ProductInformation(this.product);
  @override
  _ProductInformationState createState() => _ProductInformationState();
}

final productReference = FirebaseDatabase.instance.reference().child('libro');

class _ProductInformationState extends State<ProductInformation> {

  List<Book> items;

  String productImage;//nuevo

  @override
  void initState() {
    super.initState();
    productImage = widget.product.productImage;//nuevo
    print(productImage);//nuevo
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aplicación'),
        backgroundColor: Colors.blueAccent,
      ),
      body: Container(
        height: 800.0,
        padding: const EdgeInsets.all(20.0),
        child: Card(

          child: Column(
            children: <Widget>[
              new Text("Nombre del libro : ${widget.product.name}", style: TextStyle(fontSize: 18.0),),
              Padding(padding: EdgeInsets.only(top: 8.0),),
              Divider(),
              new Text("Area : ${widget.product.area}", style: TextStyle(fontSize: 18.0),),
              Padding(padding: EdgeInsets.only(top: 8.0),),
              Divider(),
              new Text("Aplicacion : ${widget.product.nombreApp}", style: TextStyle(fontSize: 18.0),),
              Padding(padding: EdgeInsets.only(top: 8.0),),
              Divider(),
              new Text("Package : ${widget.product.package}", style: TextStyle(fontSize: 18.0),),
              Padding(padding: EdgeInsets.only(top: 8.0),),
              Divider(),
              new Text("Descripción : ${widget.product.description}", style: TextStyle(fontSize: 18.0),),
              Padding(padding: EdgeInsets.only(top: 8.0),),
              Divider(),
              ButtonBar(
                children: <Widget>[
                  Text("Ver"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}