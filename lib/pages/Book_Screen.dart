/*import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:appeducar/Model/libro.dart';*/

/*
class PageTodos2 extends StatefulWidget {
  String title;

  PageTodos2(String titulo1){
    this.title = titulo1;
    print(title);
  }



  @override
  State<StatefulWidget> createState() {
    return _Todos2PagesState(this.title);
  }
}

  class _Todos2PagesState extends State<PageTodos2>{
  String titulo;
  _Todos2PagesState(String titulo1){
    this.titulo = titulo1;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(titulo),
      ),
    );
  }



}*/
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:appeducar/Model/libro.dart';
//nuevo para imagenes
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:date_format/date_format.dart';

File image;
String filename;

class BookScreen extends StatefulWidget {
  final Book product;
  BookScreen(this.product);
  @override
  _BookScreenState createState() => _BookScreenState();
}

final productReference = FirebaseDatabase.instance.reference().child('libro');

class _BookScreenState extends State<BookScreen> {

  List<Book> items;

  TextEditingController _nameController;
  TextEditingController _areaController;
  TextEditingController _descriptionController;
  TextEditingController _serieController;
  TextEditingController _nombreAppController;
  TextEditingController _packageController;

  //nuevo imagen
  String productImage;

  pickerCam() async {
    File img = await ImagePicker.pickImage(source: ImageSource.camera);
    // File img = await ImagePicker.pickImage(source: ImageSource.camera);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  pickerGallery() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    // File img = await ImagePicker.pickImage(source: ImageSource.camera);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  Widget divider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
      child: Container(
        width: 0.8,
        color: Colors.black,
      ),
    );
  }
  //fin nuevo imagen

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = new TextEditingController(text: widget.product.name);
    _areaController = new TextEditingController(text: widget.product.area);
    _descriptionController = new TextEditingController(text: widget.product.description);
    _serieController = new TextEditingController(text: widget.product.serie);
    _nombreAppController = new TextEditingController(text: widget.product.nombreApp);
    _packageController = new TextEditingController(text: widget.product.package);
    productImage = widget.product.productImage;
    print(productImage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Añadir Libro'),
        backgroundColor: Colors.redAccent,
      ),
      body: SingleChildScrollView(

        //height: 570.0,
        padding: const EdgeInsets.all(20.0),
        child: Card(
          child: Center(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    new Container(
                      height: 100.0,
                      width: 100.0,
                      decoration: new BoxDecoration(
                          border: new Border.all(color: Colors.redAccent)),
                      padding: new EdgeInsets.all(5.0),
                      child: image == null ? Text('Add') : Image.file(image),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 2.2),
                      child: new Container(
                        height: 100.0,
                        width: 100.0,
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.redAccent)),
                        padding: new EdgeInsets.all(5.0),
                        child: productImage == '' ? Text('Edit') : Image.network(productImage+'?alt=media'),
                      ),
                    ),
                    Divider(),
                    //nuevo para llamar imagen de la galeria o capturarla con la camara
                    new IconButton(
                        icon: new Icon(Icons.camera_alt), onPressed: pickerCam),
                    Divider(),
                    new IconButton(
                        icon: new Icon(Icons.image), onPressed: pickerGallery),
                  ],
                ),
                TextField(
                  controller: _nameController,
                  style:
                  TextStyle(fontSize: 17.0, color: Colors.black),
                  decoration: InputDecoration(
                      icon: Icon(Icons.person), labelText: 'Name'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                ),
                Divider(),
                TextField(
                  controller: _areaController,
                  style:
                  TextStyle(fontSize: 17.0, color: Colors.grey),
                  decoration: InputDecoration(
                      icon: Icon(Icons.person), labelText: 'Area'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                ),
                Divider(),
                TextField(
                  controller: _serieController,
                  style:
                  TextStyle(fontSize: 17.0, color: Colors.grey),
                  decoration: InputDecoration(
                      icon: Icon(Icons.list), labelText: 'Serie'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                ),
                Divider(),
                TextField(
                  controller: _nombreAppController,
                  style:
                  TextStyle(fontSize: 17.0, color: Colors.grey),
                  decoration: InputDecoration(
                      icon: Icon(Icons.monetization_on), labelText: 'Aplicación'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 1.0),
                ),
                Divider(),
                TextField(
                  controller: _packageController,
                  style:
                  TextStyle(fontSize: 17.0, color: Colors.grey),
                  decoration: InputDecoration(
                      icon: Icon(Icons.shop), labelText: 'Package'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 1.0),
                ),
                Divider(),
                TextField(
                  controller: _descriptionController,
                  style:
                  TextStyle(fontSize: 17.0, color: Colors.grey),
                  decoration: InputDecoration(
                      icon: Icon(Icons.shop), labelText: 'Descripción'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 1.0),
                ),
                Divider(),
                FlatButton(
                    onPressed: () {
                      //nuevo imagen
                      if (widget.product.id != null) {
                        var now = formatDate(
                            new DateTime.now(), [yyyy, '-', mm, '-', dd]);
                        var fullImageName =
                            '${_nameController.text}-$now' + '.jpg';
                        var fullImageName2 =
                            '${_nameController.text}-$now' + '.jpg';

                        final StorageReference ref =
                        FirebaseStorage.instance.ref().child(fullImageName);
                        final StorageUploadTask task = ref.putFile(image);

                        var part1 =
                            'https://firebasestorage.googleapis.com/v0/b/flutterimagen.appspot.com/o/';//esto cambia segun su firestore

                        var fullPathImage = part1 + fullImageName2;

                        productReference.child(widget.product.id).set({
                          'name': _nameController.text,
                          'area': _areaController.text,
                          'serie': _serieController.text,
                          'nombreApp': _nombreAppController.text,
                          'package': _packageController.text,
                          'description': _descriptionController.text,
                          'ProductImage': '$fullPathImage'
                        }).then((_) {
                          Navigator.pop(context);
                        });
                      } else {
                        //nuevo imagen
                        var now = formatDate(
                            new DateTime.now(), [yyyy, '-', mm, '-', dd]);
                        var fullImageName =
                            '${_nameController.text}-$now' + '.jpg';
                        var fullImageName2 =
                            '${_nameController.text}-$now' + '.jpg';

                        final StorageReference ref =FirebaseStorage.instance.ref().child(fullImageName);
                        final StorageUploadTask task = ref.putFile(image);

                        var part1 =
                            'https://firebasestorage.googleapis.com/v0/b/flutterimagen.appspot.com/o/'; //esto cambia segun su firestore

                        var fullPathImage = part1 + fullImageName2;

                        productReference.push().set({
                          'name': _nameController.text,
                          'area': _areaController.text,
                          'serie': _serieController.text,
                          'nombreApp': _nombreAppController.text,
                          'package': _packageController.text,
                          'description': _descriptionController.text,
                          'ProductImage': '$fullPathImage'
                        }).then((_) {
                          Navigator.pop(context);
                        });
                      }
                    },
                    child: (widget.product.id != null)
                        ? Text('Update')
                        : Text('Add')),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
