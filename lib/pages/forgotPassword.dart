import 'package:flutter/material.dart';
import 'package:appeducar/behaviors/hiddenScrollBehaviour.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>(); // para presentar menus contextuales con informacion al usuario



  String _email;



  bool _isSendingForgotPassword = false;


  _forgotPassword() async{
    if(_isSendingForgotPassword)return;
    setState(() {
      _isSendingForgotPassword = true;
    });

    final form = _formKey.currentState;

    if (!form.validate()) {
      // llama a cada uno de los formfields en su metodo de validate al igual que a cada uno de los de save
      _scaffoldKey.currentState.hideCurrentSnackBar();
      setState(() {
        _isSendingForgotPassword = false;
      });
      return;
    }

    form.save();

    try {
      // para hacer referencia al metodo de autenticacion de la base de datos
      await FirebaseAuth.instance.sendPasswordResetEmail(email: _email);
      _scaffoldKey.currentState.hideCurrentSnackBar();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text('Contraseña de restablecimiento de correo electrónico enviado! Revisa tu bandeja de entrada'),
          duration: Duration(seconds: 10),
      ));

    } catch (ev) {
      _scaffoldKey.currentState.hideCurrentSnackBar();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(ev.message),
        duration: Duration(seconds: 10),
        action: SnackBarAction(
          label: "Dismiss",
          onPressed: () {
            _scaffoldKey.currentState.hideCurrentSnackBar();
          },
        ),
      ));
    }
    finally{
      setState(() {
        _isSendingForgotPassword = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('¿Has olvidado tu contraseña?'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: ScrollConfiguration(
            behavior: HiddenScrollBehavior(),
            child: Form(
              key: _formKey,
                child: ListView(
                  children: <Widget>[
                    FlutterLogo(style: FlutterLogoStyle.markOnly, size: 100.0),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Text("Introduce tu dirección de correo electrónico de recuperación"),
                    ),
                    TextFormField(
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(labelText: 'Email'),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Por favor introduzca un correo electrónico válido";
                        } else {
                          return null;
                        }
                      },
                      onSaved: (val) {
                        // metodo quese llama cuando se guarda el nombre
                        setState(() {
                          _email = val;
                        });
                      },
                    ),
                  ],
                ),
            ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){
            _forgotPassword();
          },
        child: Icon(Icons.restore),
      ),
    );
  }
}
