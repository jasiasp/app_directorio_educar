/*import 'package:appeducar/pages/libro_information.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:appeducar/Model/libro.dart';
import 'dart:convert';
import 'dart:async';
import 'package:appeducar/pages/todos2.dart';*/

/*
class PageTodos extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TodosPageState();
}

class _TodosPageState extends State<PageTodos> {
  List<String> dbSeries = new List();
  List<String> dbAreas = new List();
  List<String> dbImages = new List();


  getItems() async {
    await Future.delayed(Duration(seconds: 1));
    List<String> SeriesBook = [
      'serie1',
      'serie2',
      'serie3',
      'serie4',
      'Fifth Book',
      'Sixth Book',
      'Seventh Book',
      'Eighth Book',
      'ninth Book',
      'Tenth Book'
    ];
    List<String> AreaBook = [
      'Firt Area',
      'Second Area',
      'Third Area',
      'Fourth Area',
      'Fifth Area',
      'Sixth Area',
      'Seventh Area',
      'Eighth Area',
      'ninth Area',
      'Tenth Area'
    ];

    List<String> ImageBook =[

    ];
    setState(() {
      dbSeries.addAll(SeriesBook);
      dbAreas.addAll(AreaBook);
    });
  }

  itemPress(String item) {
    print(item);
  }

  @override
  void initState() {
    getItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Aplicaciones"),
      ),
      body: dbSeries.length != 0
          ? ListView.builder(
              itemCount: dbSeries.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) => InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      child: Card(
                        elevation: 5.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0.0),
                        ),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: 55.0,
                                    height: 55.0,
                                    child: CircleAvatar(
                                      foregroundColor: Colors.blueAccent,
                                      backgroundImage: NetworkImage(
                                          "http://educar.com.co/img/bookshophome1-logo-15391118301.jpg"),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        dbSeries[index],
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        dbAreas[index],
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 13.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context)=>PageTodos2(dbSeries[index])));
                      //print(dbSeries[index]);


                    },
                  ),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
*/
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:async';
import 'package:appeducar/pages/Book_Screen.dart';
import 'package:appeducar/pages/libro_information.dart';
import 'package:appeducar/Model/libro.dart';

class ListViewProduct extends StatefulWidget {
  @override
  _ListViewProductState createState() => _ListViewProductState();
}

final productReference = FirebaseDatabase.instance.reference().child('libro');

class _ListViewProductState extends State<ListViewProduct> {
  List<Book> items;
  StreamSubscription<Event> _onProductAddedSubscription;
  StreamSubscription<Event> _onProductChangedSubscription;

  @override
  void initState() {
    super.initState();
    items = new List();
    _onProductAddedSubscription =
        productReference.onChildAdded.listen(_onProductAdded);
    _onProductChangedSubscription =
        productReference.onChildChanged.listen(_onProductUpdate);
  }

  @override
  void dispose() {
    super.dispose();
    _onProductAddedSubscription.cancel();
    _onProductChangedSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Series'),
          centerTitle: true,
          backgroundColor: Colors.redAccent,
        ),
        body: Center(
          child: ListView.builder(
              itemCount: items.length,
              padding: EdgeInsets.only(top: 3.0),
              itemBuilder: (context, position) {
                return Column(
                  children: <Widget>[
                    Divider(
                      height: 1.0,
                    ),
                    Container(
                      padding: new EdgeInsets.all(3.0),
                      child: Card(
                        child: Row(
                          children: <Widget>[
                            //nuevo imagen
                            new Container(
                              padding: new EdgeInsets.all(5.0),
                              child: '${items[position].productImage}' == ''
                                  ? Text('No image')
                                  : Image.network(
                                '${items[position].productImage}' +
                                    '?alt=media',
                                fit: BoxFit.fill,
                                height: 57.0,
                                width: 57.0,
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                  title: Text(
                                    '${items[position].name}',
                                    style: TextStyle(
                                      color: Colors.blueAccent,
                                      fontSize: 21.0,
                                    ),
                                  ),
                                  subtitle: Text(
                                    '${items[position].description}',
                                    style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontSize: 21.0,
                                    ),
                                  ),
                                  onTap: () => _navigateToProductInformation(
                                      context, items[position])),
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                              onPressed: () => _showDialog(context, position),
                            ),

                            //onPressed: () => _deleteProduct(context, items[position],position)),
                            IconButton(
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: Colors.blueAccent,
                                ),
                                onPressed: () =>
                                    _navigateToProduct(context, items[position])),
                          ],
                        ),
                        color: Colors.white,
                      ),
                    ),
                  ],
                );
              }),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          backgroundColor: Colors.redAccent,
          onPressed: () => _createNewProduct(context),
        ),
      ),
    );
  }

  //nuevo para que pregunte antes de eliminar un registro
  void _showDialog(context, position) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: Text('Are you sure you want to delete this item?'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.delete,
                color: Colors.purple,
              ),
              onPressed: () =>
                  _deleteProduct(context, items[position], position,),
            ),
            new FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _onProductAdded(Event event) {
    setState(() {
      items.add(new Book.fromSnapShot(event.snapshot));
    });
  }

  void _onProductUpdate(Event event) {
    var oldProductValue =
    items.singleWhere((product) => product.id == event.snapshot.key);
    setState(() {
      items[items.indexOf(oldProductValue)] =
      new Book.fromSnapShot(event.snapshot);
    });
  }

  void _deleteProduct(
      BuildContext context, Book product, int position) async {
    await productReference.child(product.id).remove().then((_) {
      setState(() {
        items.removeAt(position);
        Navigator.of(context).pop();
      });
    });
  }

  void _navigateToProductInformation(
      BuildContext context, Book product) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => BookScreen(product)),
    );
  }

  void _navigateToProduct(BuildContext context, Book product) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProductInformation(product)),
    );
  }

  void _createNewProduct(BuildContext context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              BookScreen(Book(null, '', '', '', '', '', '',''))),
    );
  }


}
