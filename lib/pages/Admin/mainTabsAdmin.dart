import 'package:flutter/material.dart';
import 'package:appeducar/pages/settings.dart';
import 'package:appeducar/pages/Admin/todosAdmin.dart';
import 'package:appeducar/pages/todosArchive.dart';

class MainTabsPage extends StatelessWidget{


  @override
  Widget build(BuildContext context) {



    // TODO: implement build
    return Scaffold(

      body: DefaultTabController(
          length: 3,
          child: new Scaffold(
            body:


            TabBarView(
                children: <Widget>[
                  ListViewProduct(),
                  PageTodosArchives(),
                  SettingsPage(),
                ],

            ),


            bottomNavigationBar: PreferredSize(
                child: TabBar(
                  labelColor: Theme.of(context).primaryColor,
                  labelStyle: TextStyle(fontSize: 10.0),
                  tabs: <Widget>[
                    Tab(
                      icon: Icon(Icons.list),
                      text: "Aplicaciones",
                    ),
                    Tab(
                      icon: Icon(Icons.search),
                      text: "Busqueda",
                    ),
                    Tab(
                      icon: Icon(Icons.settings),
                      text: "Ajustes",
                    ),
                  ],
                ),
                preferredSize: Size(60.0, 60.0)
            ),
          )
      ),
    );
  }

}