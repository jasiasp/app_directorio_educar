
import 'package:flutter/material.dart';
import 'package:appeducar/pages/login.dart';
import 'package:appeducar/pages/Admin/mainTabsAdmin.dart';
import 'package:appeducar/pages/user/mainTabsUser.dart';
import 'package:appeducar/pages/register.dart';
import 'package:appeducar/pages/forgotPassword.dart';

Map<String, WidgetBuilder> buildAppRoutes() {
  return {
    '/login': (BuildContext context) => LoginPage(),
    '/register': (BuildContext context) => RegisterPage(),
    '/forgotpassword': (BuildContext context) => ForgotPasswordPage(),
    '/maintabs': (BuildContext context) => MainTabsPage(),
    '/maintabs2': (BuildContext context) => MainTabsPageUser(),

  };
}
