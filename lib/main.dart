import 'package:appeducar/pages/login.dart';
import 'package:appeducar/pages/Admin/mainTabsAdmin.dart';
import 'package:flutter/material.dart';
import 'package:appeducar/Them.dart';
import 'package:appeducar/pages/register.dart';
import 'package:appeducar/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main() => runApp(TodoApp());

class TodoApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>  _TodoAppState();
}

class _TodoAppState extends State<TodoApp> {
  Widget rootPage = RegisterPage(); // para defiinir la pagina inicial

  Future<Widget> getRootPage() async =>
      await FirebaseAuth.instance.currentUser()== null
        ? LoginPage()
        : MainTabsPage();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getRootPage().then((Widget page){
      setState(() {
        rootPage = page;
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Todos App',
      home: rootPage,
      routes: buildAppRoutes(),
      theme: buildAppTheme(),
    );
  }
}
